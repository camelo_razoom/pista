var gulp = require('gulp');
var sass = require('gulp-sass');
var include = require('gulp-include');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var src = {
  scss: './scss/application.scss',
  js: './public/javascripts/application.js'
};

gulp.task('sass', function () {
  gulp.src(src.scss)
  .pipe(sass({ outputStyle: 'compressed' }))
  .pipe(rename({ extname: '.min.css' }))
  .pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('uglify', function(){
  gulp.src(src.js)
  .pipe(include())
  .pipe(uglify())
  .pipe(rename({ extname: '.min.js' }))
  .pipe(gulp.dest('./public/javascripts'));
});

gulp.task('default', ['sass', 'uglify'], function(){
  gulp.watch(['./scss/*.scss', './scss/*/*.scss'], ['sass']);
  gulp.watch(src.js, ['uglify']);
});
