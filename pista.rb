require 'sinatra'
require 'tilt/erubis'

DESTINATIONS = [
  'Rio de Janeiro',
  'Porto de Galinhas',
  'Fernando de Noronha',
  'Salvador'
]

PRODUCTS = [
  'O Melhor City Tour do Rio de Janeiro',
  'Passeio em Angra dos Reis e Ilha Grande',
  'Ilha Tour - Fernando de Noronha',
  'Bate e Volta aos Lençóis Marenhenses...'
]

configure :development do
  set :bind, '0.0.0.0'
  set :port, '3000'
end

get '/' do
  erb :home , locals: {
    controller_name: 'home',
    destinations: DESTINATIONS,
    products: PRODUCTS
  }
end

get '/activities' do
  erb :activities_index, locals: {
    controller_name: 'activities',
    products: PRODUCTS,
    query: params['q']
  }
end

get '/activities/:id' do |id|
  erb :activities_show, locals: {
    controller_name: 'activities',
    product: PRODUCTS[id.to_i],
    id: id
  }
end

get '/activities/:id/checkout' do |id|
  erb :checkout, locals: {
    controller_name: 'checkout',
    product: PRODUCTS[id.to_i]
  }
end
