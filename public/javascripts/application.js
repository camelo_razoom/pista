//=require vendor/jquery-1.12.0.min.js
//=require vendor/jquery.sticky.js

(function(){
  $(function(){
    $('a[href="#"]').click(prevent);
    $('img').on('dragstart', prevent);

    activities();
    checkout();
  });

  function prevent(e){
    e.preventDefault();
  }

  // Specific controller scripts

  function activities(){
    var $dom = $('html,body');
    var $sticky = $('.sticky');

    if($sticky.length && $(window).width() > 991){
      $sticky.sticky({ topSpacing: 10 });
    }

    $('.back-to-top').click(function(){
      $dom.animate({ scrollTop: 0 }, 400);
    });
  }

  function checkout(){
    var $phoneModel = $('.phone-model');
    var phoneLength = $phoneModel.length;

    $phoneModel = $phoneModel.clone().cleanInputs().addClass('to-fade')

    $('.add-phone').click(function(){
      var $self = $(this).closest('div');
      $self.fadeOut(200, function(){
        $('.phone-model').last().after($phoneModel.clone());
        $('.phone-model.to-fade').fadeIn(200, function(){
          $(this).removeClass('to-fade');

          if(phoneLength++ < 2)
          $self.fadeIn(200);
          else
          $self.remove();
        });
      });
    });
  }

  // jQuery extends

  $.fn.extend({
    cleanInputs: function(){
      return this.each(function(){
        $(this).find('input').val('');
      });
    }
  });
}());
